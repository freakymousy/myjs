import Search from './models/Search';
import * as searchView from './views/serachView';
import {elements,renderLoader, clearLoader} from "./views/base";
// Global State of the app
// Search Object
// Current Recipe Object
// Shopping List Object
// Liked recipe
const state = {};
const controlSearch = async () =>{
      //1) Get query from the view
    const query = searchView.getInput();


    if(query){
        //2) New Search object and add to state
       state.search = new Search(query);

        //3) Prepare UI for result
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.searchRes);


        //4) Search for recipes
        await state.search.getResult();


        //5) Render result on UI
        clearLoader();
        searchView.renderResult(state.search.result);
    }
};

elements.searchForm.addEventListener('submit', e=>{
    e.preventDefault();
    controlSearch();
});

//search.getResult();
elements.searchResPages.addEventListener('click', e=> {
    const btn = e.target.closest('.btn-inline');
    if(btn){
        const goToPage = parseInt(btn.dataset.goto, 10);
        searchView.clearResults();
        searchView.renderResult(state.search.result ,goToPage);
    }
});


//3b3e3e3429da9ab52634a5dac4922caa
// https://www.food2fork.com/api/search