/*
/!*
//Lecture: Let and const



//ES5
var name5 = 'Jane Smith';
var age5 = 23;
name5 = 'Jane Miller';
console.log(name5);


//ES6
/!*
const name6 = 'Jane Smith';
let age = 23;
name6 = 'Jane Miller';
console.log(name6);
*!/



//ES5

function driverLicense5(passedTest) {
    if(passedTest){
        var firstName = 'John';
        var yearOfBirth = 1990;
    }
    console.log(firstName + ',born in ' + yearOfBirth + ' is now officially allowed to drive a car.');

}
driverLicense5(true);

//ES6

function driverLicense6(passedTest) {
    if(passedTest){
        let firstName = 'John';
        const yearOfBirth = 1990;
    }
    console.log(firstName + ',born in ' + yearOfBirth + ' is now officially allowed to drive a car.');

}
driverLicense6(true);
*!/










//Lecture: Strings

/!*
let firstName = 'John';
let lastName = 'Smith';
const  yearOfBirth = 1990;
function calcAge(year) {
    return 2016 - year;
}


//ES5
console.log('This is ' + firstName + ' ' + lastName + '.He was born in ' + yearOfBirth + '.Today , he is ' + calcAge(yearOfBirth) + ' years old');


//ES6

console.log(`This is ${firstName} ${lastName}. He was born in ${yearOfBirth}.Today he is ${calcAge(yearOfBirth)} years old`);

const  n = `${firstName} ${lastName}`;
console.log(n.startsWith(`J`));
console.log(n.endsWith(`th`));
console.log(n.includes(` `));
console.log(`${firstName} `.repeat(5));
*!/


//Lecture: Arrow Function

/!*const years = [1990,1965,1982,1937];

//ES5
var ages5 = years.map(function (el) {
     return 2016 - el;
});
console.log(ages5);

//ES6
let ages6 = years.map(el => 2016 - el);
console.log(ages6);


ages6 = years.map((el, index) => `Age element ${index + 1}: ${2016 - el}.`);
console.log(ages6);

ages6 = years.map((el , index)=>{
    const now = new Date().getFullYear();
    const age = now - el;
    return `Age element ${index + 1}: ${age}.`
});
console.log(ages6);*!/



// Arrow Function 2

//ES5
var box5 = {
    color: 'green',
    position: 1,
    clickMe: function () {
        var self = this;
        document.querySelector('.green').addEventListener('click', function () {
            var str = 'This is box number ' + self.position + ' and it is '+ self.color;
            alert(str);
        });
    }
};
//box5.clickMe();

//ES6
const box6 = {
    color: 'green',
    position: 1,
    clickMe: function () {
        document.querySelector('.green').addEventListener('click', ()=> {
            var str = 'This is box number ' + this.position + ' and it is '+ this.color;
            alert(str);
        });
    }
};
 box6.clickMe();

/!*
const box66 = {
    color: 'green',
    position: 1,
    clickMe:  () => {
        document.querySelector('.green').addEventListener('click', ()=> {
            var str = 'This is box number ' + this.position + ' and it is '+ this.color;
            alert(str);
        });
    }
};
box66.clickMe();
*!/



function Person(name) {
    this.name = name;
}

//ES5
/!*
Person.prototype.myFriends5 = function (friends) {
      var arr = friends.map(function (el) {
         return this.name + ' is friends with ' + el;
      });
    console.log(arr);
};

var friends = ['Bob','Jane','Mark'];
new
Person('John').myFriends5(friends);

*!/


//Lecture: Destructing

//ES5

/!*
var john = ['John',26];
// var name = john[0];
// var age = john[1];



//ES6
const [name, age] = ['John',29];
console.log(name);
console.log(age);
*!/



//Lecture: Arrays

const boxes = document.querySelectorAll('.box');

//ES5
var boxesArr5 = Array.prototype.slice.call(boxes);
boxesArr5.forEach(function (cur) {
    cur.style.backgroundColor = 'dodgerblue';
});

//ES6
const boxesArr6 = Array.from(boxes);
boxesArr6.forEach(cur => cur.style.backgroundColor = 'dodgerblue');

//ES5
/!*
for(var i = 0; i<boxesArr5.length; i++){

    if(boxesArr5[i].className === 'box blue'){
        // continue;
        break;
    }
    boxesArr5[i].textContent = 'I changed to blue!';
}
*!/


//ES6

for (const cur of boxesArr6){
    if(cur.className.includes('blue')){
        continue;
    }
    cur.textContent = 'I changed it to Blue!';
}

//ES5
var ages = [12, 17, 8, 21, 14, 11];

var full = ages.map(function (cur) {
    return cur >= 18;
});
console.log(full);

console.log(full.indexOf(true));
console.log(ages[full.indexOf(true)]);


//ES6

console.log(ages.findIndex(cur => cur >= 18));
console.log(ages.find(cur => cur >= 18));

*/



//Lecture: Spread Operator

/*
function addFourAges(a, b, c, d) {
    return a + b + c + d;
}

var sum1 = addFourAges(18, 20, 12, 21);
console.log(sum1);


//ES5
var ages = [18, 30, 12, 21];
var sum2 = addFourAges.apply(null,ages);
console.log(sum2);

//ES6
const sum3 = addFourAges(...ages);
console.log(sum3);


const familySmith = ['John','Jane','Mark'];
const familyMiller = ['Mary','Bob','Ann'];
const bigFamily = [...familyMiller,'Lily', ...familySmith];
console.log(bigFamily);



const h = document.querySelector('h1');
const boxes = document.querySelectorAll('.box');
const all = [h,...boxes];

Array.from(all).forEach(cur => cur.style.color = 'purple');
*/


//Lecture: Rest Parameters

//ES5

/*
function isFullAge5() {
    //console.log(arguments);
    var argsArr = Array.prototype.slice.call(arguments);

    argsArr.forEach(function (cur) {
        console.log((2016 - cur) >= 18);
    })
}


// isFullAge5(1990, 1999, 1965);


//ES6
function isFullAge6(...years) {
        years.forEach(cur => console.log((2016 - cur)>= 18));
}
isFullAge6(1990, 1999, 1965);
*/


//Lecture: Maps
/*

const question = new Map();
question.set('question','What is the official name of the latest major JavaScript version');
question.set(1,'ES5');
question.set(2,'ES6');
question.set(3,'ES2015');
question.set(4,'ES7');
question.set('correct',3);
question.set(true,'Correct Answer :D');
question.set(false,'Wrong,Please try again :3');

console.log(question.get('question'));
//console.log(question.size);


/!*if (question.has(4)){
    question.delete(4);
}*!/
//question.clear();

//question.forEach((value,key) => console.log(`This is ${key},and it's set to ${value}`));

for (let [key, value]of question.entries()){
    if(typeof (key)=== 'number'){
        console.log(`Answer ${key}: ${value}`);
    }
}

const ans = parseInt( prompt('Write the correct answer'));

console.log(question.get(ans === question.get('correct')));
*/


//Lecture: Classes

//ES5

/* var Person5  = function (name, yearOfBirth , job) {
     this.name = name;
     this.yearOfBirth = yearOfBirth;
     this.job = job;
 };
 Person5.prototype.calculateAge = function () {
     var age = new Date().getFullYear() - this.yearOfBirth;
     console.log(age);
 };

 var john5 = new Person5('John',1990,'Teacher');

 //ES6
class Person6 {
    constructor(name, yearOfBirth, job){
        this.name = name;
        this.yearOfBirht = yearOfBirth;
        this.job = job;
    }

    calculateAge(){
        var age = new Date().getFullYear() - this.yearOfBirth;
        console.log(age);
    }

    static greetings(){
        console.log(`hey there`);
    }
}

const john6 = new Person6('John',1990,'Teacher');


Person6.greetings();*/


//Lecture: Classes and SubClasses

var Person5  = function (name, yearOfBirth , job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
};
Person5.prototype.calculateAge = function () {
    var age = new Date().getFullYear() - this.yearOfBirth;
    console.log(age);
};

var john5 = new Person5('John',1990,'Teacher');

var Athlete5 = function (name, yearOfBirth , job,olympicGames, medals) {
    Person5.call(this, name,yearOfBirth,job);
    this.olympicGames = olympicGames;
    this.medals = medals;
};

Athlete5.prototype = Object.create(Person5.prototype);
Athlete5.prototype.wonMedal = function(){
    this.medals++;
    console.log(this.medals);
};

var johnAthlete5 = new Athlete5('John',1990,'Swimmer',3,10);

johnAthlete5.calculateAge();
johnAthlete5.wonMedal();



//ES6
class Person6 {
    constructor(name, yearOfBirth, job){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }

    calculateAge(){
        var age = new Date().getFullYear() - this.yearOfBirth;
        console.log(age);
    }
}


class Athlete6 extends Person6{
    constructor(name, yearOfBirth, job, olympicGames, medals){
        super(name,yearOfBirth,job);
        this.olympicgames = olympicGames;
        this.medals = medals;

    }

    wonMedal(){
        this.medals++;
        console.log(this.medals++);
    }
}

const  johnAthlete6 = new Athlete6('John',1990,'Swimmer',3,10);
johnAthlete6.wonMedal();
johnAthlete6.calculateAge();





