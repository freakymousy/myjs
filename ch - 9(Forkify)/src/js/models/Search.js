import axios from 'axios';


export default class Search {
    constructor(query){
        this.query = query;
    }
    async getResult() {
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        const key = '3b3e3e3429da9ab52634a5dac4922caa';
        try {
            const res = await axios(`${proxy}https://www.food2fork.com/api/search?key=${key}&q=${this.query}`);
            this.result = res.data.recipes;
            //console.log(this.result);
        } catch (error) {
            alert(error);
        }
    }
}





